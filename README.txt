PURPOSE
To provide the means to display product kits on checkout, checkout review, order views, and invoices.

CHALLENGE
When a product kit is added to the cart/order, all items are stored invidually, and by default Ubercart displays them individually.

NOTES
1.  **Very important** This module does not affect the display of orders or the cart out of the box.  To alter the display of these 
    items, you must insert the relevant function supplied by this module in the appropriate location.  See 'sample_application.inc'
    for an example.  Note these "locations" can vary considerably on a site-by-site basis (different theme functions, pane overrides, etc),
    so rather then making all the alterations in this module, I include the sample application to give an idea of what needs 
    to be done.  
    
2.  Exercise caution when using the alter functions.  They should only be applied immediately prior to display of the cart/order, and
    the altered cart/order should not be used in subsquent operations during page creation.
   
3.  The module only works for orders created while it was enabled.  Older orders don't store information re: product kits that we
    need in order to know whether to display them; this module adds this information when an order is created.
    