<?php
/**
 *  Consolidates each valid product kit into a single product representation in order object
 *
 *  @param $order:  Array of order object's products
 */
function pkitdisplay_alter_order(&$order) {   
  $kits = array ();          
  //First pass through - collect kit component info. in $kits array
  foreach ($order->products as $product) {  
    //Only collect data for product kits that aren't mutable 
    if (isset($product->data['kit_title']) && $product->data['kit_type'] != UC_PRODUCT_KIT_MUTABLE) {      
      $kit_unid = $product->data['unique_id'];      
      if (!isset($kits[$kit_unid])) {        
        $kits[$kit_unid] = array ();        
      }
      
      //Pass kit component price and cost through alter process
      $context = array(
        'revision' => 'altered',    
        'type' => 'order_product',
        'subject' => array(
          'order' => $order,
          'subject' => $product,
          'node' => node_load($product->nid),
        ),
      );      
      $price_info = array (
        'price' => $product->price,
        'qty' => $product->qty,
      );     
      $kits[$kit_unid][$product->nid] = array (
        'qty' => $product->qty,
        'price' => uc_price($price_info, $context),
        'cost' => uc_price($product->cost, $context),
      );      
      $kits[$kit_unid][$product->nid]['desc'] = _pkitdisplay_store_desc($product->qty, $product->title,
        uc_product_get_description($product));                           
    }
  } 
  
  //Second pass through - check integrity of kits and alter $order->products  
  if (!empty($kits)) {    
    $good_kits = array ();    
    $skip_kits = array ();
    foreach ($order->products as $o_pid => $product) {      
      if (isset($product->data['kit_title']) && !in_array($product->data['unique_id'], $skip_kits)) {        
        $kit_unid = $product->data['unique_id'];
        //If we haven't yet verified integrity, do so and alter product if it passes validation
        //Otherwise we just remove the product kit component
        if (!in_array($kit_unid, $good_kits)) {          
          do {           
            $good = 1;        
            if ($product->qty % $product->data['kit_components'][$product->nid] == 0) {              
              $qty = $product->qty / $product->data['kit_components'][$product->nid];                            
            }
            else {              
              $good = 0;              
              break;
            }
            
            //Make sure all necessary kit components are present
            if (count($kits[$kit_unid]) != count($product->data['kit_components'])) {
              $good = 0;
              break;
            }
            
            //Make sure all kit component quantities are in correct proportion
            foreach ($product->data['kit_components'] as $kit_component_id => $kit_component_qty) {
              if ($qty * $kit_component_qty != $kits[$kit_unid][$kit_component_id]['qty']) {               
                $good = 0;
                break 2;
              }                
            }           
          } while (FALSE);          
          
          if ($good) {                 
            $good_kits[] = $kit_unid;
            $order->products[$o_pid]->title = $product->data['kit_title'];            
            $order->products[$o_pid]->data['pkit_desc'] = array ($kits[$kit_unid][$product->nid]['desc']);           
            $order->products[$o_pid]->price = $kits[$kit_unid][$product->nid]['price']; 
            $order->products[$o_pid]->cost = $kits[$kit_unid][$product->nid]['cost'];          
            foreach ($kits[$kit_unid] as $kit_component_id => $kit_component_info) {
              if ($kit_component_id != $product->nid) {           
                $order->products[$o_pid]->price += $kit_component_info['price'];
                $order->products[$o_pid]->cost += $kit_component_info['cost'];                         
                $order->products[$o_pid]->data['pkit_desc'][] = $kit_component_info['desc'];                
              }              
            }           
            
            $order->products[$o_pid]->price /= $qty;
            $order->products[$o_pid]->qty = $qty;
            $order->products[$o_pid]->nid = $product->data['kit_id'];            
            $order->products[$o_pid]->model = 'N/A';
            //Default invoice template hardcoded to output attributes; unset so partial attributes not shown
            unset($order->products[$o_pid]->data['attributes']);
          }
          else {
            $skip_kits[] = $kit_unid;
          }
        }
        else {
          unset($order->products[$o_pid]);
        }        
      }
    }   
  } 
}

/*
 *  Consolidates each valid product kit into a single product representation in cart array
 *
 *  @param $products:  The cart contents
 */
function pkitdisplay_alter_cart(&$products) {     
  $kits = array ();  
  $skip_kits = array ();  
  //First pass through - collect kit data and validate
  foreach ($products as $product) {        
    if (isset($product->data['unique_id']) && !in_array($product->data['unique_id'], $skip_kits)) {
      $kit_unid = $product->data['unique_id'];
      //Load kit data if necessary
      if (!isset($kits[$kit_unid])) {                
        if (($kit = node_load($product->data['kit_id'])) && $kit->mutable != UC_PRODUCT_KIT_MUTABLE) {             
          //Avoid need for deep clone of cart contents by creating new object with the properties we need
          $kit_temp = new stdClass;          
          $kit_temp->components = array ();                    
          foreach ($kit->products as $kit_component_id => $kit_component) {          
            $kit_temp->components[$kit_component_id] = $kit_component->qty;          
          }
          
          //Verify kit component belongs, and that the quantity makes sense
          if (isset($kit_temp->components[$product->nid])) {
            if ($product->qty % $kit_temp->components[$product->nid] == 0) {              
              $kit_temp->qty = $product->qty / $kit_temp->components[$product->nid];
              $kit_temp->title = $kit->title;                    
              $kit_temp->comp_count = 1;              
              $kit_temp->data = array ();              
              $kit_temp->data['pkit_desc'] = array (_pkitdisplay_store_desc($product->qty, $product->title,
                uc_product_get_description($product)));
              $kit_temp->data['kit_type'] = $kit->mutable;                    
              $kit_temp->nid = $product->nid;
              //Pass the original price through the alter process           
              $context = array (
                'revision' => 'altered',
                'type' => 'cart_item',
                'field' => 'price',
                'subject' => array (
                  'cart' => $products,
                  'cart_item' => $product,
                  'node' => node_load($product->nid),                  
                ),
              );            
              $price_info = array (
                'price' => $product->price,
                'qty' => $product->qty,
              );              
              $kit_temp->price = uc_price($price_info, $context) / $kit_temp->qty;             
              $kits[$kit_unid] = $kit_temp;
            }
            else {
              $skip_kits[] = $kit_unid;
            }
          }
        }
        else {            
          $skip_kits[] = $kit_unid;
        }           
      }
      else {        
        //Verify kit component belongs, and that the quantity makes sense
        if (isset($kits[$kit_unid]->components[$product->nid])) {
          if ($kits[$kit_unid]->qty * $kits[$kit_unid]->components[$product->nid] == $product->qty) {                      
            $kits[$kit_unid]->data['pkit_desc'][] = _pkitdisplay_store_desc($product->qty, $product->title,
              uc_product_get_description($product));            
            $kits[$kit_unid]->comp_count++;
            //Pass the original price through the alter process
            $context['subject']['cart_item'] = $product;
            $context['subject']['node'] = node_load($product->nid);
            $price_info = array (
              'price' => $product->price,
              'qty' => $product->qty,
            );            
            $kits[$kit_unid]->price += uc_price($price_info, $context) / $kits[$kit_unid]->qty;           
          }
          else {           
            unset($kits[$kit_unid]);
            $skip_kits[] = $kit_unid;
          }
        }
      }        
    }      
  } 
  
  //Verify # kit components from node == # kit components in cart
  foreach ($kits as $kit_store => $kit) {
    if (count($kit->components) != $kit->comp_count) {
      unset($kits[$kit_store]);
    }
  } 

  //Second loop through $products to make any necessary alterations    
  if (!empty($kits)) {   
    foreach ($products as $pid => $product) {
      //Check again to make sure product is a valid kit component         
      if (isset($product->data['unique_id']) && isset($kits[$product->data['unique_id']]->components[$product->nid])) {       
        //Replace values in first kit component we find; unset the rest  
        if (!isset($kits[$product->data['unique_id']]->added)) {
          $kits[$product->data['unique_id']]->added = 1;
          $products[$pid] = $kits[$product->data['unique_id']];                  
        }
        else {
          unset($products[$pid]);
        }
      }
    }
  }      
}

/*
 *  Returns associative array with product kit description info.
 */
function _pkitdisplay_store_desc($qty, $title, $desc) {
  return array (
    'qty' => $qty,
    'title' => $title,
    'desc' => $desc,
  );
}

/****************Drupal hook implementations*************************/

/**
 *  Implementation of hook_theme() 
 */
function pkitdisplay_theme() {
  return array (
    'pkitdisplay' => array (
      'arguments' => array ('desc' => array ()),
    ),
  );
}

/****************Ubercart hook implementations***********************/

/**
 *  Implementation of hook_order_product_alter()
 *
 *  We need to permanently store additional information about product kit components in the order so that we can decide
 *  whether to display them individually or together
 * 
 *  @param $product:  The $product object
 *  @param $order:  The $order object
 *  @see: uc_product_kit_order_product_alter
 */
function pkitdisplay_order_product_alter(&$product, $order) { 
  //Continue only if the product may be a kit component
  if (!isset($product->data['kit_id'])) {    
    return;
  }      
  
  //If the item has a unique ID, it was added via the cart.  If added via order edit, we need to create the unique ID. 
  //Do not add unique ID to previously saved kits - they may be corrupted from previous order modifcations  
  //The method in this blcok assumes a single product is added at a time
  if (!isset($product->data['unique_id'])) {
    static $id = 0;     
    if (!isset($product->order_product_id)) {      
      if (!$id) {
        $id = uniqid('', TRUE);
      }
      
      $product->data['unique_id'] = $id;
    }
  }
  
  //Add kit data to each valid kit components, for all product kit types
  static $kits_meta = array ();    
  if (isset($product->data['unique_id']) && !isset($product->data['kit_title'])) {   
    $kit_id = $product->data['kit_id'];    
    //Load and store needed kit data
    if (!isset($kits_meta[$kit_id])) {
      //Make sure the product is still part of the kit
      if ($kit = node_load($kit_id, NULL, TRUE)) {       
        $kits_meta[$kit_id] = array ();
        $kits_meta[$kit_id]['kit_title'] = $kit->title;
        $kits_meta[$kit_id]['kit_type'] = $kit->mutable;
        $kits_meta[$kit_id]['kit_components'] = array ();         
        foreach ($kit->products as $kit_component_id => $kit_component) {          
          $kits_meta[$kit_id]['kit_components'][$kit_component_id] = $kit_component->qty;          
        }
      }      
    }
        
    //Add the data to the product if it is still a component of the kit
    if (isset($kits_meta[$kit_id]['kit_components'][$product->nid])) {
      $product->data = array_merge($product->data, $kits_meta[$kit_id]);
    }
  }
}

/*
 *  
 *  Implementation of hook_product_description_alter()
 *  
 *  Ensure only the product kit description gets displayed
 */
function pkitdisplay_product_description_alter(&$description, $product) { 
  //Add description, and delete any other descriptions that may be present  
  if (isset($product->data['pkit_desc'])) {   
    $description = array (
       'pkitdisplay' => array (
         '#data' => $product->data['pkit_desc'],         
         '#kit_type' => $product->data['kit_type'],
         '#theme' => 'pkitdisplay',
       ),
    );      
  }
}

/*
 *  Returns attributes for product kits
 */
function theme_pkitdisplay($element) {  
  $output = '';  
  foreach ($element['#data'] as $kit_component) {
    if ($element['#kit_type'] == UC_PRODUCT_KIT_UNMUTABLE_WITH_LIST) {
      $output .= $kit_component['qty'] . 'x ' . $kit_component['title'] . $kit_component['desc'];  
    }
  } 
  
  return $output;
}
