<?php
/* 
 *  Theme override function for checkout page product pane; when enabled, product kits (not the kit components) should appear on the 
 *  checkout pane
 *  Two changes are made to the default function, as highlighted below 
 *    1.  Use alter function to modify cart contents; 
 *    2.  Ensure product kits are price via uc_price using "original" revision (kit components already put through price alter process)  
 *  @see theme_cart_review_table in uc_cart/uc_cart_checkout_pane 
 */
function pkitdisplay_cart_review_table($show_subtotal = TRUE) {
  $subtotal = 0;

  // Set up table header.
  $header = array(
    array('data' => t('Qty'), 'class' => 'qty'),
    array('data' => t('Products'), 'class' => 'products'),
    array('data' => t('Price'), 'class' => 'price'),
  );

  $context = array(); 

  // Set up table rows.
  $contents = uc_cart_get_contents(); 
//***** Start add code - CHANGE 1
  pkitdisplay_alter_cart($contents);
//***** End add code
  
  foreach ($contents as $item) { 
    $price_info = array(
      'price' => $item->price,
      'qty' => $item->qty,
    );  
//****** Start modify code = CHANGE 2
    // Kit components already passed through the alter process    
    $context['revision'] = isset($item->data['pkit_desc']) ? 'original' : 'altered';
//***** End modify code
    $context['type'] = 'cart_item';
    $context['subject'] = array(
      'cart' => $contents,
      'cart_item' => $item,
      'node' => node_load($item->nid),
    );

    $total = uc_price($price_info, $context);
    $subtotal += $total;

    $description = check_plain($item->title) . uc_product_get_description($item);

    // Remove node from context to prevent the price from being altered.
    $context['revision'] = 'themed-original';
    $context['type'] = 'amount';
    unset($context['subject']);
    $rows[] = array(
      array('data' => t('@qty&times;', array('@qty' => $item->qty)), 'class' => 'qty'),
      array('data' => $description, 'class' => 'products'),
      array('data' => uc_price($total, $context), 'class' => 'price'),
    );
  }

  // Add the subtotal as the final row.
  if ($show_subtotal) {
    $context = array(
      'revision' => 'themed-original',
      'type' => 'amount',
    );
    $rows[] = array(
      'data' => array(array('data' => '<span id="subtotal-title">' . t('Subtotal:') . '</span> ' . uc_price($subtotal, $context), 'colspan' => 3, 'class' => 'subtotal')),
      'class' => 'subtotal',
    );
  }

  return theme('table', $header, $rows, array('class' => 'cart-review'));
}
